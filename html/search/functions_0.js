var searchData=
[
  ['collatz_5feval',['collatz_eval',['../Collatz_8c_09_09.html#a0b0d3827a619c18aa4d96b8ee8b1c47d',1,'collatz_eval(int i, int j):&#160;Collatz.c++'],['../Collatz_8h.html#a0b0d3827a619c18aa4d96b8ee8b1c47d',1,'collatz_eval(int i, int j):&#160;Collatz.c++']]],
  ['collatz_5feval_5fhelper',['collatz_eval_helper',['../Collatz_8c_09_09.html#a571c07bd5c5f37c3125c05106304cc31',1,'Collatz.c++']]],
  ['collatz_5fprint',['collatz_print',['../Collatz_8c_09_09.html#aeda0b7ea3e40e1e7487ccc436f33a559',1,'collatz_print(ostream &amp;w, int i, int j, int v):&#160;Collatz.c++'],['../Collatz_8h.html#aeda0b7ea3e40e1e7487ccc436f33a559',1,'collatz_print(ostream &amp;w, int i, int j, int v):&#160;Collatz.c++']]],
  ['collatz_5fread',['collatz_read',['../Collatz_8c_09_09.html#a2772f8a734aeab48332eb3b282f991ba',1,'collatz_read(const string &amp;s):&#160;Collatz.c++'],['../Collatz_8h.html#a2772f8a734aeab48332eb3b282f991ba',1,'collatz_read(const string &amp;s):&#160;Collatz.c++']]],
  ['collatz_5fsolve',['collatz_solve',['../Collatz_8c_09_09.html#a0ac646d2122741f9a9a52201bf9551cc',1,'collatz_solve(istream &amp;r, ostream &amp;w):&#160;Collatz.c++'],['../Collatz_8h.html#a0ac646d2122741f9a9a52201bf9551cc',1,'collatz_solve(istream &amp;r, ostream &amp;w):&#160;Collatz.c++']]],
  ['cycle_5flength',['cycle_length',['../Collatz_8c_09_09.html#a47c4f5d3e657532339f84d16ef18a4d1',1,'Collatz.c++']]]
];
