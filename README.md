# CS371p: Object-Oriented Programming Collatz Repo

* Name: Ryan Resma

* EID: rmr3429

* GitLab ID: rresma99

* HackerRank ID: rresma99

* Git SHA: 23f5517424044d6cdc7254352009b4cdbb2a2700

* GitLab Pipelines: https://gitlab.com/rresma99/cs371p-collatz/pipelines

* Estimated completion time: 10.0

* Actual completion time: 8.0

* Comments: Thank you for setting up such a nice makefile and environment w/ Docker!